ios-sdk
=======

This project contains developer examples demonstrating Telerik UI for iOS features. You can get Telerik UI for iOS product at: http://www.telerik.com/ios-ui

Telerik UI for iOS features:
- Charting library
- Easy to use API
- Maintenance and Support

Here are the examples at a glance: 

![Demo app](http://blogs.telerik.com/images/default-source/ios-team/ios-samples-app.gif?sfvrsn=0.6434930243995041)

More precisely, the demo application demonstrates:

- The available chart types:
<br/>
![Chart types](http://blogs.telerik.com/images/default-source/ios-team/telerik-ui-for-ios-series.gif?sfvrsn=2)

- Animations:
<br/>
![Chart animations](http://blogs.telerik.com/images/default-source/ios-team/telerik-ios-animations-all.gif?sfvrsn=2)

- Style customization:
<br/>
![Chart style customization](http://blogs.telerik.com/images/default-source/ios-team/telerik-ui-for-ios-customizations.png?sfvrsn=2)

- Annotations:
<br/>
![Chart annotations](http://blogs.telerik.com/images/default-source/ios-team/telerik-ui-for-ios-annotations.gif?sfvrsn=2)